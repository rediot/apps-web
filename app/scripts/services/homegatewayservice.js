'use strict';
/**
 * @ngdoc service
 * @name iotapp.GroupDeviceService
 * @description
 * # GroupDeviceService
 * Factory in the iotapp.
 */
angular.module('iotapp')
        .factory('HomeGateWayService', ['$resource', 'ServiceConfig', 'SessionService',
            function ($resource, ServiceConfig, SessionService) {
                var apiUrl = ServiceConfig.getUrl();
                var sessionId = SessionService.getSessionId();
                return $resource(null,
                        {
                            sessionId: sessionId
                        },
                {
                    getListHG: {
                        method: 'GET',
                        url: apiUrl + '?method=gateway_getuserhomelist&session=:sessionId'
                    },
                    getUrlHG: function ()
                    {
                        var resp = this.getListHG({
                        }, function () {
                            var urlHG = resp.data.homes[0].name;
                            return urlHG;
                        });
                        return "";
                    }
                });
            }]);
