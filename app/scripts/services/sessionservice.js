'use strict';

/**
 * @ngdoc service
 * @name vngdoorviewApp.SessionService
 * @description
 * # SessionService
 * Factory in the vngdoorviewApp.
 */
angular.module('iotapp')
        .factory('SessionService', ['$cookieStore', '$window', function ($cookieStore, $window) {

                return {
                    logIn: function (sessionId, userName) {
                        
                        var user = {sessionId: sessionId, userName: userName};
                        $cookieStore.put('user', user);
                    },
                    logOut: function () {
                        $cookieStore.remove('user');
                        $window.location.reload();
                    },
                    getUser: function () {
                        return $cookieStore.get('user');
                    },
                    getSessionId: function ()
                    {
                        var user = this.getUser();
                        if (user !== undefined) {
                            return user.sessionId;
                        }
                        return "";
                    },
                    getSessionUname: function ()
                    {
                        var user = this.getUser();
                        if (user !== undefined) {
                            return user.userName;
                        }
                        return "";
                    }
                };
            }]);
