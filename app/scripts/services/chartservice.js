'use strict';
/**
 * @ngdoc service
 * @name iotapp.GroupDeviceService
 * @description
 * # GroupDeviceService
 * Factory in the iotapp.
 */
angular.module('iotapp')
        .factory('ChartService', ['$resource', 'ServiceConfig', 'SessionService',
            function ($resource, ServiceConfig, SessionService) {
                var apiUrl = ServiceConfig.getUrl();
                var sessionId = SessionService.getSessionId();
//                var sessionId = '9a53002058ec7c50dc76f47617c2ba16';
                return $resource(null,
                        {
                            sessionId: sessionId
                        },
                {
                    getChartData: {
                        method: 'GET',
                        url: apiUrl + '/account?method=login_signin&username=trungnt4&password=123456'
                    }
                });
            }]);
