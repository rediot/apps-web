'use strict';
/**
 * @ngdoc service
 * @name iotapp.ErrorCMDService
 * @description
 * # ErrorCMDService
 * Factory in the iotapp.
 */
angular.module('iotapp')
	.factory('ErrorCMDService', ['$modal',
	    function ($modal) {

		return {
		    displayError: function (msg) {
			$modal.open({
			    templateUrl: 'views/errorcmd.html',
			    controller: 'ErrorcmdCtrl',
			    resolve: {
				msg: function () {
				    return msg;
				}
			    }
			});
		    }

		}
		;
	    }]);
