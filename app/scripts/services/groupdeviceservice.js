'use strict';
/**
 * @ngdoc service
 * @name iotapp.GroupDeviceService
 * @description
 * # GroupDeviceService
 * Factory in the iotapp.
 */
angular.module('iotapp')
		.factory('GroupDeviceService', ['$resource', 'ServiceConfig', 'SessionService',
			function ($resource, ServiceConfig, SessionService) {
				var apiUrl = ServiceConfig.getUrl();
				var sessionId = SessionService.getSessionId();
				return $resource(null,
						{
							sessionId: sessionId
						},
				{
					getGroupDevice: {
						method: 'GET',
						url: apiUrl + '?method=gateway_getgrouplist&session=:sessionId&homeId=:homeId'
					},
					getDeviceInGroup: {
						method: 'GET',
						url: apiUrl + '?method=gateway_getdevicelist&session=:sessionId&homeId=:homeId&groupId=:groupId'
					},
					addGroup: {
						method: 'GET',
						url: apiUrl + '?method=gateway_homeaddgroup&session=:sessionId&homeId=:homeId&name=:name'
					},
					editGroup: {
						method: 'GET',
						url: apiUrl + ''
					},
					deleteGroup: {
						method: 'GET',
						url: apiUrl + '?method=gateway_removegroup&session=:sessionId&groupId=:groupId'
					},
					deleteDevice: {
						method: 'GET',
						url: apiUrl + '?method=gateway_removedevice&session=:sessionId&deviceId=:deviceId'
					},
					addDevice: {
						method: 'GET',
						url: apiUrl + '?method=gateway_groupadddevice&session=:sessionId&homeId=:homeId&groupId=:groupId&name=:name&description=:description&type=:type&avatar=http://c1.f36.img.vnecdn.net/2015/07/18/dalatsuongsom11366702477500x0-1437191898_180x108.jpg'
					},
					moveDevice: {
						method: 'GET',
						url: apiUrl + '?method=gateway_movedevice&session=:sessionId&deviceId=:deviceId&groupIdTo=:groupIdTo'
					}
				});
			}]);
