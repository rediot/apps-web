'use strict';
/**
 * @ngdoc service
 * @name iotapp.UserService
 * @description
 * # UserService
 * Factory in the iotapp.
 */
angular.module('iotapp')
.factory('UserService', ['$resource', 'ServiceConfig', 'SessionService',
function ($resource, ServiceConfig, SessionService) {
    var apiUrl = ServiceConfig.getUrl();
    var sessionId = SessionService.getSessionId();

    return $resource(null, {
        sessionId: sessionId
    }, {
        logIn: {
            method: 'GET',
            url: apiUrl + '/account?method=login_signin&username=:user&password=:password'
        },
        addUser: {
            method: 'GET',
            url: apiUrl + '?user=:user&password=:password&method=user.add'
        },
        getUser: {
            method: 'GET',
            url: apiUrl + '?method=user.get'
        },
        updateUserPassword: {
            method: 'GET',
            url: apiUrl + '?&password=:password&method=user.update.password' + '&newPassword=:newPassword'
                    + '&repeatPassword=:repeatPassword'
        },
        listUser: {
            method: 'GET',
            url: apiUrl + '?method=user.list'
        },
        getUserBySession: {
            method: 'GET',
            url: apiUrl + '?method=user.getBySession'
        }
    });
}]);
