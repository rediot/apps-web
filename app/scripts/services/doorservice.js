'use strict';
/**
 * @ngdoc service
 * @name iotapp.DoorService
 * @description
 * # DoorService
 * Factory in the iotapp.
 */
angular.module('iotapp')
	.factory('DoorService', ['$resource', 'ServiceConfig', 'SessionService',
	    function ($resource, ServiceConfig, SessionService) {

		var apiUrl = ServiceConfig.getUrl();
		var sessionId = SessionService.getSessionId();
		return $resource(null,
			{
			    sessionId: sessionId
			},
		{
		    addDoor: {
			method: 'GET',
			url: apiUrl + '?name=:name&description=:description&method=door.add'
		    },
		    getDoor: {
			method: 'GET',
			url: apiUrl + '?uuid=:uuid&major=:major&minor=:minor&method=door.get'
		    },
		    updateDoor: {
			method: 'GET',
			url: apiUrl + '?uuid=:uuid&major=:major&minor=:minor&method=door.update' +
				'&description=:description' + '&name=:name'
		    },
		    removeDoor: {
			method: 'GET',
			url: apiUrl + '?uuid=:uuid&major=:major&minor=:minor&method=door.remove'
		    },
		    listDoor: {
			method: 'GET',
			url: apiUrl + '?&method=door.lists'
		    }

		});
	    }]);
