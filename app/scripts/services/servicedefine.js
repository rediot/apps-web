'use strict';
/**
 * @ngdoc service
 * @name myAppApp.ServiceConfig
 * @description
 * # ServiceConfig
 * Factory in the myAppApp.
 */
angular.module('iotapp')
        .factory('ServiceDefine', function () {

            return {
                getListDeviceDefined: function ()
                {
                    var list = "[{\"code\":\"1\",\"name\":\"den\",\"property\":[\"onOff\"],\"description\":\"miêu tả đèn\"},{\"code\":\"2\",\"name\":\"cua\",\"property\":[\"openClosed\"],\"description\":\"miêu tả cửa\"},{\"code\":\"3\",\"name\":\"quat\",\"property\":[\"curvalue\",\"minValue\",\"maxValue\"],\"description\":\"miêu tả quạt\"}]";
                    return list;
                }
            };
        });
