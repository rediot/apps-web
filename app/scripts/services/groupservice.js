'use strict';
/**
 * @ngdoc service
 * @name iotapp.GroupService
 * @description
 * # GroupService
 * Factory in the iotapp.
 */
angular.module('iotapp')
.factory('GroupService', ['$resource', 'ServiceConfig', 'SessionService', function ($resource, ServiceConfig, SessionService) {
	var apiUrl = ServiceConfig.getUrl();
	var sessionId = SessionService.getSessionId();
	return $resource(null, {sessionId: sessionId}, {
	    addGroup: {
		method: 'GET',
		url: apiUrl + '?name=:name&description=:description&method=group.create'

	    },
	    listGroup: {
		method: 'GET',
		url: apiUrl + '?method=group.list'

	    },
	    getGroup: {
		method: 'GET',
		url: apiUrl + '?method=group.get'

	    },
	    updateGroup: {
		method: 'GET',
		url: apiUrl + '?name=:name&description=:description&method=group.update'

	    },
	    registerUser: {
		method: 'GET',
		url: apiUrl + '?method=group.register.user'

	    },
	    registerUserByName: {
		method: 'GET',
		url: apiUrl + '?method=group.register.userByName'

	    },
	    registerDoor: {
		method: 'GET',
		url: apiUrl + '?method=group.register.door'

	    },
	    unregisterUser: {
		method: 'GET',
		url: apiUrl + '?method=group.unregister.user'

	    },
	    unregisterDoor: {
		method: 'GET',
		url: apiUrl + '?method=group.unregister.door'

	    }
	});
}]);
