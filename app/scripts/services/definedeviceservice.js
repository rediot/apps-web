'use strict';
/**
 * @ngdoc service
 * @name iotapp.DoorService
 * @description
 * # DoorService
 * Factory in the iotapp.
 */
angular.module('iotapp')
        .factory('DefineDeviceService', ['$resource', 'ServiceConfig', 'SessionService',
            function ($resource, ServiceConfig, SessionService) {

                var apiUrl = ServiceConfig.getUrl();
                var sessionId = SessionService.getSessionId();
                return $resource(null,
                        {
                            sessionId: sessionId
                        },
                {
//                    getGroupDevice: {
//                        method: 'GET',
//                        url: 'http://iot.minkb.com/api/v1?method=gateway_getgrouplist&session=9a53002058ec7c50dc76f47617c2ba16'
//                    }
                });
            }]);