'use strict';

/**
 * @ngdoc service
 * @name iotapp.ServiceConfig
 * @description
 * # ServiceConfig
 * Factory in the iotapp.
 */
angular.module('iotapp')
		.factory('ServiceConfig', function () {
			
			return {
				getUrl: function ()
				{
					var url = 'http://iot.minkb.com/api/v1';
					return url;
				},
				getUrlLogin: function ()
				{
					var url = '/login.html';
					return url;
				},
				getParam: function (param)
				{
				
					//url = http://iot.minkb.com/#/hgid/home/groupid
					var p_ram;
					var url = window.location.href.split('#/')[1];
					switch (param) {
						case 'hgid':
							param = url.split('/')[0];
							break;
						case 'action':
							param = url.split('/')[1];
							break;
						case 'groupId':
							param = url.split('/')[2];
							break;						
						case 'deviceId':
							param = url.split('/')[3];
							break;
						default:
							break;
					}				
//					var url = window.location.href.split('#/')[1];
//					var param = url.split('/')[1];
					return param;
				}
			};
		});
