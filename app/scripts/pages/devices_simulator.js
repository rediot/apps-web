angular.module('iotapp', []).controller('userCtrl', function ($scope) {
    $scope.listdevices = [
        {id: 1, type: 'light', devicetype: 'actuator', device: "63888", param: "1", desct: "Lobby Wall Light #1", control: "ON", status: "light-on.png"},
        {id: 2, type: 'light', devicetype: 'actuator', device: "63889", param: "1", desct: "Bedroom Downlights", control: "ON", status: "light-on.png"},
        {id: 3, type: 'door', devicetype: 'sensor', device: "25667", param: "1", desct: "Lobby Entry Door", control: "OPEN", status: "open-door.png"},
        {id: 4, type: 'signal', devicetype: 'sensor', device: "39333", param: "1", desct: "Lobby Motion Detection", control: "ON", status: "red-signal.png"}
    ];
    $scope.changeStt = function (id) {
        switch ($scope.listdevices[id - 1].type) {
            case 'light':
                if ($scope.listdevices[id - 1].status == 'light-off.png') {
                    $scope.listdevices[id - 1].status = 'light-on.png';
                    $scope.listdevices[id - 1].control = 'ON';
                }
                else
                {
                    $scope.listdevices[id - 1].status = 'light-off.png';
                    $scope.listdevices[id - 1].control = 'OFF';
                }
                break;
            case 'door':
                if ($scope.listdevices[id - 1].status == 'open-door.png') {
                    $scope.listdevices[id - 1].status = 'closed-door.png';
                    $scope.listdevices[id - 1].control = 'CLOSE';
                }
                else
                {
                    $scope.listdevices[id - 1].status = 'open-door.png';
                    $scope.listdevices[id - 1].control = 'OPEN';
                }
                break;
            case 'signal':
                if ($scope.listdevices[id - 1].status == 'red-signal.png') {
                    $scope.listdevices[id - 1].status = 'green-signal.png';
                    $scope.listdevices[id - 1].control = 'OFF';
                }
                else
                {
                    $scope.listdevices[id - 1].status = 'red-signal.png';
                    $scope.listdevices[id - 1].control = 'ON';
                }
                break;
            default:

                break;
        }
    };
})