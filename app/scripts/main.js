'use strict';
/**
 * @ngdoc overview
 * @name upphotoviewApp
 * @description
 * # upphotoviewApp
 *
 * Main module of the application.
 */
angular.module('iotapp', [
	'ngResource',
	'ngRoute',
	'ngCookies',
	'ui.bootstrap',
	'smart-table'
])
		.config(function ($routeProvider) {
			$routeProvider
					.when('/:hgid/:action/:groupid', {
						templateUrl: 'views/home.html',
						controller: 'GroupDeviceCtrl',
						activetab: 'home'
					})
					.when('/:hgid/addnewgroup', {
						templateUrl: 'views/addnewgroup.html',
						controller: 'addnewgroupCtrl',
						activetab: 'home'
					})
					.when('/:hgid/definedevice', {
						templateUrl: 'views/definedevice.html',
						controller: 'definedeviceCtrl',
						activetab: 'home'
					})
					.when('/:hgid/search', {
						templateUrl: 'views/search.html',
						controller: 'SearchCtrl',
						activetab: 'home'
					})
					.when('/detailgroup', {
						templateUrl: 'views/detailgroup.html',
						controller: 'detailgroupCtrl',
						activetab: 'home'
					})
					.when('/:hgid/:action/:groupid/:deviceid', {
						templateUrl: 'views/detaildevice.html',
						controller: 'DetailDeviceCtrl',
						activetab: 'home'
					})
					.when('/addnewdevice', {
						templateUrl: 'views/addnewdevice.html',
						controller: 'addnewdeviceCtrl',
						activetab: 'home'
					})
					.when('/test', {
						templateUrl: 'views/devides-simulator.html',
						controller: 'homeCtrl',
						activetab: 'doors'
					})
					.when('/users', {
						templateUrl: 'views/userlist.html',
						controller: 'UserListCtrl',
						activetab: 'users'
					})
					.when('/groups', {
						templateUrl: 'views/grouplist.html',
						controller: 'GroupListCtrl',
						activetab: 'groups'
					})
					.when('/errorCMD', {
						templateUrl: 'views/errorcmd.html',
						controller: 'ErrorcmdCtrl'
					})
					.otherwise({
						//redirectTo: '/login'
					});
		})

		.controller('IndexCtrl', ['$scope', '$cookieStore', '$modal', '$window',
			'SessionService', 'ErrorCMDService', 'UserService',
			function ($scope, $cookieStore, $modal, $window,
					SessionService, ErrorCMDService, UserService) {

				$scope.showLogin = function () {
					$modal.open({
						templateUrl: 'views/login.html',
						controller: 'LoginCtrl'
					});
				};
				$scope.isLoggedIn = function () {
					return SessionService.getUser() !== undefined;
				};
				$scope.logOut = function () {
					SessionService.logOut();
				};
				$scope.getProfile = function () {
					var resp = UserService.getUserBySession({
					}, function () {
						if (resp.error_code === 0)
						{
							$modal.open({
								templateUrl: 'views/userdetail.html',
								controller: 'UserDetailCtrl',
								resolve: {
									cb: function () {
										return undefined;
									},
									user: function () {
										return resp.data;
									}
								}
							});
						}
						else
						{
							ErrorCMDService.displayError('No Permission');
						}
					});
				};
				$scope.getUserMenu = function ()
				{
					return $cookieStore.get('user');
				};
				$scope.userMenu = $scope.getUserMenu();
				$scope.getMenuTabs = function () {
					var tabs;
					tabs = [{name: 'home', path: '#/home', title: 'Home'}];
					tabs.push({name: 'doors', path: '#/doors', title: 'Doors'});
					tabs.push({name: 'users', path: '#/users', title: 'Users'});
					tabs.push({name: 'groups', path: '#/groups', title: 'Groups'});
					return tabs;
				};
				$scope.menutabs = $scope.getMenuTabs();
			}]);
