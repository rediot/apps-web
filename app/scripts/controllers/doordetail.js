'use strict';
var app = angular.module('iotapp');
app.controller('DoorDetailCtrl', ['$scope', '$modalInstance', 'door', 'cb',
    'DoorService', 'ErrorCMDService',
    function ($scope, $modalInstance, door, cb,
            DoorService, ErrorCMDService) {

        $scope.hasDoor = (door !== undefined);
        $scope.init = function ()
        {
            $scope.newDoor = {uuid: '', major: '', minor: '', id: ''};
            if ($scope.hasDoor)
            {
                $scope.localDoor = {
                    name: door.name,
                    description: door.description,
                    id: door.id,
                    uuid: door.uuid,
                    major: door.major,
                    minor: door.minor
                };
            }


        };
        $scope.init();


        $scope.ok = function () {
            $modalInstance.close($scope.localDoor);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };


        $scope.confirm = function () {

            if ($scope.hasDoor)
            {
                var resp = DoorService.updateDoor({
                    name: $scope.localDoor.name,
                    description: $scope.localDoor.description,
                    uuid: $scope.localDoor.uuid,
                    major: $scope.localDoor.major,
                    minor: $scope.localDoor.minor,
                    id: $scope.localDoor.id
                }, function () {
                    if (resp.error_code === 0)
                    {
                        $scope.ok();
                        cb();
                    }
                    else
                    {
                        ErrorCMDService.displayError(resp.error_message);
                    }

                });
            }
            else
            {
                var resp = DoorService.addDoor({
                    name: $scope.newDoor.name,
                    description: $scope.newDoor.description,
                    uuid: $scope.newDoor.uuid,
                    major: $scope.newDoor.major,
                    minor: $scope.newDoor.minor

                }, function () {
                    if (resp.error_code === 0) {
                        $scope.ok();
                        cb();
                    }
                    else {
                        $scope.showMsg('Add failed');
                    }

                });
            }
        }
        ;
    }
]);