'use strict';

var app = angular.module('iotapp');
app.controller('definedeviceCtrl', ['$scope', 'SessionService',
    function ($scope, SessionService) {
        $scope.deviceProperty = '';
        $scope.createDeviceCode = function () {
            $scope.deviceCode = parseInt($scope.listdevicedefined[$scope.listdevicedefined.length - 1].code) + 1;
            $scope.deviceName = '';
            $scope.deviceDesc = '';
            $scope.deviceProperty = '';
        };
        $scope.editDeviceDefined = function (code, name, description, property) {
            $scope.deviceCode = code;
            $scope.deviceName = name;
            $scope.deviceDesc = description;
            $scope.deviceProperty = '';
            for (var i = 0, max = property.length; i < max; i++) {
                if ($scope.deviceProperty.length > 0) {
                    $scope.deviceProperty = $scope.deviceProperty + '\n' + property[i];
                }
                else
                {
                    $scope.deviceProperty = property[i];
                }

            }
        };

        $scope.init = function ()
        {
            var sessionId = SessionService.getSessionId();
            if (sessionId == '') {
                window.location.href = "login.html";
            }
            $scope.title = 'Define new device';
            $scope.listdevicedefined = [
                {code: '1', name: 'den', property: ['onOff'], description: 'miêu tả đèn'},
                {code: '2', name: 'cua', property: ['openClosed'], description: 'miêu tả cửa'},
                {code: '3', name: 'quat', property: ['curvalue', 'minValue', 'maxValue'], description: 'miêu tả quạt'}
            ];
        };
        $scope.init();

    }
]);
