'use strict';

angular.module('iotapp')
	.controller('RegisterUserCtrl', ['$scope', '$modalInstance', 'selectedGroup', 'cb',
	    'GroupService', 'ErrorCMDService',
	    function ($scope, $modalInstance, selectedGroup, cb,
		    GroupService, ErrorCMDService) {

		$scope.init = function ()
		{

		};
		$scope.init();

		$scope.ok = function () {
		    $modalInstance.close($scope.localDoor);
		};
		$scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		};
		$scope.confirm = function () {

		    if ($scope.selectedUser === undefined) {
			ErrorCMDService.displayError('missing params');
			return;
		    }

		    var memberResp = GroupService.registerUserByName({
			groupId: selectedGroup, userName: $scope.selectedUser
		    },
		    function () {

			if (memberResp.error_code === 0) {
			    $scope.selectedUser = undefined;
			    $scope.ok();
			    cb();
			}
			else
			{
			    ErrorCMDService.displayError(memberResp.error_message);
			}
		    });
		};
	    }]);
