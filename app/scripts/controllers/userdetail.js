'use strict';

var app = angular.module('iotapp');
app.controller('UserDetailCtrl', ['$scope', '$resource', '$modalInstance', '$window', 'cb', 'user',
    'UserService', 'SessionService',
    function ($scope, $resource, $modalInstance, $window, cb, user,
	    UserService, SessionService) {

	$scope.hasUser = (user !== undefined);
	$scope.init = function ()
	{
	    $scope.newUser = {uname: '', password: ''};
	    if ($scope.hasUser)
	    {
		$scope.localUser = {
		    uname: user.name,
		    passwd: '',
		    newPasswd: '',
		    repeatPasswd: '',
		    groups: user.groups,
		    uid: user.id
		};

		if (user.viewAdmin)
		{
		    $scope.passTitle = 'Password Admin:';
		}
		else
		{
		    $scope.passTitle = 'Password:';
		}
	    }
	};
	$scope.init();
	$scope.confirm = function () {

	    if ($scope.hasUser) {

		var resp = UserService.updateUserPassword({
		    password: $scope.localUser.passwd,
		    newPassword: $scope.localUser.newPasswd,
		    repeatPassword: $scope.localUser.repeatPasswd,
		    userId: $scope.localUser.uid

		}, function () {
		    if (resp.error_code === 0) {
			$scope.ok();
		    }
		    else {
			$scope.showMsg('Add failed');
		    }
		});
	    }
	    else {
		var resp = UserService.addUser({
		    user: $scope.newUser.uname,
		    password: $scope.newUser.passwd
		}, function () {
		    if (resp.error_code === 0) {
			$scope.ok();
			cb();
		    }
		    else {
			$scope.showMsg('Add failed');
		    }

		});
	    }
	};
	$scope.ok = function () {
	    $modalInstance.close('Success');
	};
	$scope.cancel = function () {
	    $modalInstance.dismiss();
	};
	$scope.msgList = [];
	$scope.clearMsg = function () {
	    $scope.msgList = [];
	};
	$scope.showMsg = function (msg) {
	    $scope.msgList = [{type: 'danger', content: msg}];
	}
	;
    }
]);