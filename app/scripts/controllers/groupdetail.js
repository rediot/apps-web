'use strict';
/**
 * @ngdoc function
 * @name iotapp.controller:GroupdetailctrlCtrl
 * @description
 * # GroupdetailctrlCtrl
 * Controller of the myApp
 */
angular.module('iotapp')
	.controller('GroupDetailCtrl', ['$scope', '$modalInstance', 'group', 'cb',
	    'SessionService', 'GroupService', 'ErrorCMDService',
	    function ($scope, $modalInstance, group, cb,
		    SessionService, GroupService, ErrorCMDService) {

		$scope.hasGroup = (group !== undefined);
		$scope.init = function ()
		{
		    $scope.newGroup = {
			name: '',
			description: '',
			flags: '',
			dateCreated: '',
			dateUpdated: '',
			groupId: ''
		    };

		    if ($scope.hasGroup)
		    {
			$scope.localGroup = {
			    name: group.name,
			    description: group.description,
			    flags: group.flags,
			    dateCreated: group.dateCreated,
			    dateUpdated: group.dateUpdated,
			    groupId: group.id
			};
		    }

		};
		$scope.init();

		$scope.ok = function () {
		    $modalInstance.close($scope.localDoor);
		};
		$scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		};
		$scope.confirm = function () {
		    if ($scope.hasGroup)
		    {
			var resp = GroupService.updateGroup({
			    name: $scope.localGroup.name,
			    description: $scope.localGroup.description,
			    groupId: $scope.localGroup.groupId
			}, function ()
			{
			    if (resp.error_code === 0)
			    {
				$scope.ok();
				cb();
			    }
			    else
			    {
				ErrorCMDService.displayError(resp.error_message);
			    }

			});
		    }
		    else
		    {
			var resp = GroupService.addGroup({
			    name: $scope.newGroup.name,
			    description: $scope.newGroup.description
			}, function () {
			    if (resp.error_code === 0)
			    {
				$scope.ok();
				cb();
			    }
			    else
			    {
				$scope.showMsg('Add failed');
			    }
			});
		    }
		};
	    }]);
