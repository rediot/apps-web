'use strict';

var app = angular.module('iotapp');
app.controller('LoginCtrl', ['$scope', '$modal', '$resource', '$cookieStore', '$window',
    'SessionService', 'ServiceConfig', 'HomeGateWayService', 'GroupDeviceService', 'UserService', 'ErrorCMDService',
    function ($scope, $modal, $resource, $cookieStore, $window,
            SessionService, ServiceConfig, HomeGateWayService, GroupDeviceService, UserService, ErrorCMDService) {
        $scope.localUser = {uname: '', password: ''};
        $scope.login = function () {
            if ($scope.localUser.uname == '') {
                $scope.msg = '(*) Login failed <Username is null!>';
            }
            var resp = UserService.logIn({
                user: $scope.localUser.uname,
                password: $scope.localUser.passwd

            }, function () {
                if (resp.error === 0) {
                    SessionService.logIn(resp.data.session, $scope.localUser.uname);
                    var sessionId = resp.data.session;
                    var resplistHG = HomeGateWayService.getListHG({
                        sessionId: sessionId
                    }, function () {
                        var hgid = resplistHG.data.homes[0].id;
                        var groupiddefault;
                        var respGroupDevice = GroupDeviceService.getGroupDevice({
                            sessionId: sessionId,
                            homeId: hgid
                        }, function () {
                            groupiddefault = respGroupDevice.data[0].id;
                            window.location.href = "/#/" + hgid + "/" + groupiddefault;
							 
                        });

                    });
                } else {
                    $scope.msg = '(*) Login failed <' + resp.message + '>';
                }
            });
        };
    }
]);