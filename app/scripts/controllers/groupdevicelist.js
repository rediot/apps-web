'use strict';

var app = angular.module('iotapp');
app.controller('GroupDeviceCtrl', ['$scope', '$cookieStore', 'GroupDeviceService', 'ServiceDefine', 'SessionService', 'HomeGateWayService', 'ServiceConfig',
	function ($scope, $cookieStore, GroupDeviceService, ServiceDefine, SessionService, HomeGateWayService, ServiceConfig) {
		$scope.title = 'Home screen';
		$scope.status = 'ON';
		function init_stt_img()
		{
			for (var i = 0, maxData = $scope.data.length; i < maxData; i++) {
				for (var j = 0, max = $scope.data[i].devices.length; j < max; j++) {
					switch ($scope.data[i].devices[j].deviceType) {
						case 100:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = "light-on.png";
								$scope.data[i].devices[j].stt = 'ON';
							}
							else
							{
								$scope.data[i].devices[j].img = 'light-off.png';
								$scope.data[i].devices[j].stt = 'OFF';
							}
							$scope.data[i].devices[j].showImg = true;
							$scope.data[i].devices[j].showVal = false;
							break;
						case 105:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = 'open-door.png';
								$scope.data[i].devices[j].stt = 'OPEN';
							}
							else
							{
								$scope.data[i].devices[j].img = 'closed-door.png';
								$scope.data[i].devices[j].stt = 'CLOSE';
							}
							$scope.data[i].devices[j].showImg = true;
							$scope.data[i].devices[j].showVal = false;
							break;
						case 106:
							$scope.data[i].devices[j].img = '';
							$scope.data[i].devices[j].stt = 'VIEW CHART';
							$scope.data[i].devices[j].showImg = false;
							$scope.data[i].devices[j].showVal = true;
							break;
						case 107:
							$scope.data[i].devices[j].img = '';
							$scope.data[i].devices[j].stt = 'VIEW CHART';
							$scope.data[i].devices[j].showImg = false;
							$scope.data[i].devices[j].showVal = true;
							break;
						default:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = 'open-door.png';
								$scope.data[i].devices[j].stt = 'OPEN';
							}
							else
							{
								$scope.data[i].devices[j].img = 'closed-door.png';
								$scope.data[i].devices[j].stt = 'CLOSE';
							}
							break;
					}
				}
			}
		}
		$scope.init = function ()
		{
			$scope.action = "home";
			iniButtonAllBule();
			//get session
			var sessionId = SessionService.getSessionId();
			if (sessionId == '') {
				window.location.href = ServiceConfig.getUrlLogin();
			}

			var resplistHG = HomeGateWayService.getListHG({
			}, function () {
				var hgid = resplistHG.data.homes[0].id;
				$scope.hgid = hgid;
				if (hgid == '') {
					window.location.href = ServiceConfig.getUrlLogin();
				}
				else
				{
					var respGroupDevice = GroupDeviceService.getGroupDevice({
						homeId: hgid
					}, function () {
						$scope.data = respGroupDevice.data;
						init_stt_img();
						iniGroup();
						iniViewDeviceInGroup();
					});
				}
			});

		};
		$scope.init();

		function iniViewDeviceInGroup()
		{
			var groupId = $scope.groupIdDefault;
			var deviceType;
			for (var i = 0, maxData = $scope.data.length; i < maxData; i++) {
				if ($scope.data[i].id == groupId) {
					deviceType = $scope.data[i].devices[0].deviceType;
					$scope.nameviewchart = $scope.data[i].devices[0].name;
					$scope.deviceID = $scope.data[i].devices[0].id;
					$scope.deviceName = $scope.data[i].devices[0].name;
					$scope.description = $scope.data[i].devices[0].description;
					$scope.value = $scope.data[i].devices[0].properties.value;
					switch (deviceType) {
						case 100:
							$(".showChart").hide();
							$(".showValue").show();
							break;
						case 105:
							$(".showChart").hide();
							$(".showValue").show();
							break;
						case 106:
							$(".showChart").show();
							$(".showValue").hide();
							linechartIni();
							iniButtonFirstGray();
							break;
						case 107:
							$(".showChart").show();
							$(".showValue").hide();
							linechartIni();
							iniButtonFirstGray();
							break;

						default:

							break;
					}
				}
			}

		}

		function iniGroup()
		{
			var groupId = ServiceConfig.getParam('groupId');
			$scope.groupIdDefault = groupId;
			var checkGroupId = 0;
			$scope.listgroup = $scope.data;



			for (var i = 0, max = $scope.data.length; i < max; i++) {
				$scope.data[i].active = '';
				if (groupId == $scope.data[i].id) {
					$scope.data[i].active = 'active';
					$scope.deviceingroup = $scope.data[i].devices;
					$scope.totaldeviceingroup = $scope.data[i];
					$scope.GroupID = $scope.data[i].id;
					$scope.GroupName = $scope.data[i].name;
					checkGroupId = 1;
				}

			}
			if (checkGroupId == 0) {
				$scope.data[0].active = 'active';
				$scope.deviceingroup = $scope.data[0].devices;
				$scope.totaldeviceingroup = $scope.data[0];
				$scope.GroupID = $scope.data[0].id;
				$scope.GroupName = $scope.data[0].name;
			}
		}
		$scope.changeGroup = function (id) {
			$scope.init();
		};

		var lineChartDataWeek = {
			labels: ["2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0"]
				}
			]

		};
		var lineChartDataMonth = {
			labels: ["2015-08-01", "2015-08-03", "2015-08-03", "2015-08-04", "2015-08-05", "2015-08-06", "2015-08-07", "2015-08-08", "2015-08-09", "2015-08-10", "2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "12", "100", "0", "11", "34", "12", "0", "12", "100", "0"]
				}
			]

		};
		var lineChartDataYear = {
			labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
				}
			]

		};

		function linechartIni()
		{
			var lineChartDataDate = {
				labels: ["00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", ],
				datasets: [
					{
						label: "My First dataset",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0", "12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
					}
				]

			};
			$('#canvas').remove();
			$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
			var ctx = document.getElementById("canvas").getContext("2d");
			window.myLine = new Chart(ctx).Line(lineChartDataDate, {
				responsive: true
			});
		}
		$scope.action = function (id, onOff)
		{
			iniButtonFirstGray();
			var deviceType;

			for (var i = 0, max1 = $scope.data.length; i < max1; i++) {
				for (var j = 0, max = $scope.data[i].devices.length; j < max; j++) {
					if ($scope.data[i].devices[j].id == id) {
						deviceType = $scope.data[i].devices[j].deviceType;
						$scope.nameviewchart = $scope.data[i].devices[j].name;
						switch (deviceType) {
							case 100:
								$(".showChart").hide();
								$scope.valDisplay = "none";
								if ($scope.data[i].devices[j].properties.status == '0') {
									$scope.data[i].devices[j].img = 'light-on.png';
									$scope.data[i].devices[j].properties.status = '1';
									$scope.data[i].devices[j].stt = 'ON';
								}
								else
								{
									$scope.data[i].devices[j].img = 'light-off.png';
									$scope.data[i].devices[j].properties.status = '0';
									$scope.data[i].devices[j].stt = 'OFF';
								}
								break;
							case 105:
								$(".showChart").hide();
								$scope.valDisplay = "none";
								if ($scope.data[i].devices[j].properties.status == '0') {
									$scope.data[i].devices[j].img = 'open-door.png';
									$scope.data[i].devices[j].properties.status = '1';
									$scope.data[i].devices[j].stt = 'OPEN';
								}
								else
								{
									$scope.data[i].devices[j].img = 'closed-door.png';
									$scope.data[i].devices[j].properties.status = '0';
									$scope.data[i].devices[j].stt = 'CLOSE';
								}
								break;
							case 106:
								$(".showChart").show();
								//Ve bieu do
								$scope.valDisplay = "";
								linechartIni();
								animatedScroll();
								//iniButtonFirstGray();

								break;
							case 107:
								$(".showChart").show();
								//Ve bieu do
								$scope.valDisplay = "";
								linechartIni();
								animatedScroll();
								//iniButtonFirstGray();

								break;
							default:
								break;
						}
					}
				}
			}

		};

		function animatedScroll()
		{
			scroll(0, $(document).height());
		}

		$scope.getGroupInformation = function (id, name) {
			$scope.groupIdif = id;
			$scope.groupNameif = name;
		};
		$scope.getDeviceInformation = function (id, name) {
			$scope.deviceIdif = id;
			$scope.deviceNameif = name;
		};
		$scope.deletegroup = function () {
			var id = $scope.groupIdif;
			var resp = GroupDeviceService.deleteGroup({
				//gán tham so tai day.
				groupId: id
			}, function () {
				var resp = GroupDeviceService.getGroupDevice({
				}, function () {
					$scope.data = resp.data;
					init_stt_img();
				});
			});
		};
		$scope.deletedevice = function () {
			var deviceId = $scope.deviceIdif;
			var GroupID = $scope.GroupID;
			var resp = GroupDeviceService.deleteDevice({
				//gán tham so tai day.
				//id: id
				deviceId: deviceId
			}, function () {

				var resp = GroupDeviceService.getGroupDevice({
				}, function () {
					$scope.init();
				});
			});
			$window.location.reload();
		};
		$scope.addgroup = function () {
			var homeId = '17';
			var name = $scope.groupName;
			if (homeId.length > 0 & name.length > 0) {
				var resp = GroupDeviceService.addGroup({
					homeId: homeId,
					name: name
				}, function () {
					var resp = GroupDeviceService.getGroupDevice({
					}, function () {
						$scope.data = resp.data;
						init_stt_img();
					});
				});
			}
			else
			{
				alert('data null');
			}
		};
		$scope.adddevice = function (GroupID) {
			var homeId = '17';
			var deviceName = $scope.deviceName;
			deviceName = deviceName.replace(' ', '+');
			var description = $scope.description;
			description = description.replace(' ', '+');
			var GroupID = GroupID;
			var deviceType = $("#deviceType").val();
			if (homeId.length > 0 & deviceName.length > 0) {
				var resp = GroupDeviceService.addDevice({
					homeId: homeId,
					groupId: GroupID,
					description: description,
					type: deviceType,
					name: deviceName
				}, function () {
//					var respdata = GroupDeviceService.getGroupDevice({
//					}, function () {
//						$scope.init();
//					});
				});
			}
			else
			{
				alert('data null');
			}
			$scope.init();
		};
		$scope.movedevice = function (GroupID) {
			var homeId = '17';
			var groupIdTo = $("#groupTo").val();
			var movedeviceId = $('input[name=deviceselected]:checked').val();
			if (groupIdTo.length > 0 & movedeviceId.length > 0) {
				var resp = GroupDeviceService.moveDevice({
					deviceId: movedeviceId,
					groupIdTo: groupIdTo
				}, function () {
					$scope.movedeviceName = '';
					var resp = GroupDeviceService.getGroupDevice({
					}, function () {
						$scope.init();
					});
				});
			}
			else
			{
				alert('data null');
			}
		};
		$scope.iniInfo = function () {
			if ($('input[name=deviceselected]:checked').val() + "" != 'undefined') {
				$scope.modal_movedevice = 'modal_movedevice';

				for (var i = 0, max = $scope.data.length; i < max; i++)
				{
					for (var j = 0, max1 = $scope.data[i].devices.length; j < max1; j++) {

						if ($scope.data[i].devices[j].id == $('input[name=deviceselected]:checked').val()) {

							$scope.movedeviceName = $scope.data[i].devices[j].name;
						}
					}
				}
			}
			else
			{
				$scope.modal_movedevice = '';
				$scope.warning = 'modal_movedevice';
			}

		};

		$scope.selectgroupid = function () {
			var groupid = $("#listgroup").val();
			for (var i = 0, max = $scope.data.length; i < max; i++) {
				if ($scope.data[i].id == groupid) {
					$scope.groupname = $scope.data[i].name;
					break;
				}
			}
		};
		$scope.selectdeviceid = function () {

		};
		$scope.selectGroupTo = function () {

		};
		$scope.changeViewChart = function (time)
		{

			iniButtonAllBule();
			switch (time) {
				case 'day':
					$scope.day = 'btn btn-minw btn-rounded btn-default';
					linechartIni();
					break;
				case 'week':
					$scope.week = 'btn btn-minw btn-rounded btn-default';
//                    $('#canvas').replaceWith('<canvas id="chart" width="100" height="600"></canvas>');
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataWeek, {
						responsive: true
					});
					break;
				case 'month':
					$scope.month = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataMonth, {
						responsive: true
					});
					break;
				case 'year':
					$scope.year = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataYear, {
						responsive: true
					});
					break;
				case 'realtime':
					$scope.realtime = 'btn btn-minw btn-rounded btn-default';
					liveChart();
					break;
				default:
					break;
			}
			animatedScroll();
		};
		function iniButtonFirstGray()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-default';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		function iniButtonAllBule()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-info';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		$scope.viewdetail = function (id)
		{

		};

		$scope.gotoTop = function ()
		{
			scroll(0, 0);
		};
		function liveChart() {
			var count = 10;
			var data = {
				labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
				datasets: [
					{
						label: "My First dataset",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: [65, 59, 90, 81, 56, 45, 30, 200, 3, 37]
					}
				]
			};
			// this is ugly, don't judge me
			var updateData = function (oldData) {
				var labels = oldData["labels"];
				var dataSetA = oldData["datasets"][0]["data"];
				var dataSetB = oldData["datasets"][1]["data"];

				labels.shift();
				count++;
				labels.push(count.toString());
				var newDataA = dataSetA[9] + (20 - Math.floor(Math.random() * (41)));
				var newDataB = dataSetB[9] + (20 - Math.floor(Math.random() * (41)));
				dataSetA.push(newDataA);
				dataSetB.push(newDataB);
				dataSetA.shift();
				dataSetB.shift();
			};

			var optionsAnimation = {
				//Boolean - If we want to override with a hard coded scale
				scaleOverride: true,
				//** Required if scaleOverride is true **
				//Number - The number of steps in a hard coded scale
				scaleSteps: 10,
				//Number - The value jump in the hard coded scale
				scaleStepWidth: 10,
				//Number - The scale starting value
				scaleStartValue: 0
			};

			// Not sure why the scaleOverride isn't working...
			var optionsNoAnimation = {
				animation: false,
				//Boolean - If we want to override with a hard coded scale
				scaleOverride: true,
				//** Required if scaleOverride is true **
				//Number - The number of steps in a hard coded scale
				scaleSteps: 20,
				//Number - The value jump in the hard coded scale
				scaleStepWidth: 50,
				//Number - The scale starting value
				scaleStartValue: 0
			};

			//Get the context of the canvas element we want to select
			var ctx = document.getElementById("myChart").getContext("2d");
			var optionsNoAnimation = {animation: false}
			var myNewChart = new Chart(ctx);
			myNewChart.Line(data, optionsAnimation);

			setInterval(function () {
				updateData(data);
				myNewChart.Line(data, optionsNoAnimation);
			}, 2000);
		}
	}
]);
