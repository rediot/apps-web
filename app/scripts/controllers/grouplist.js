'use strict';
var app = angular.module('iotapp');
app.controller('GroupListCtrl', ['$scope', '$route', '$modal',
    'GroupService', 'ErrorCMDService', 'SessionService',
    function ($scope, $route, $modal,
	    GroupService, ErrorCMDService, SessionService) {

	$scope.choiceGroup = function (group)
	{
	    $scope.selectedGroup = group.id;
	    $scope.getGroup();
	};
    
	$scope.addClassMember = function (member)
	{
	    $scope.selectedMember = member.id;
	};

	$scope.releaseClassMember = function (member)
	{
	    $scope.selectedMember = -1;
	};

	$scope.addClassDoor = function (door)
	{
	    $scope.selectedDoor = door.id;
	};

	$scope.releaseClassDoor = function (door)
	{
	    $scope.selectedDoor = -1;
	};

	$scope.viewGroupDetail = function (groupId) {
	    if (!$scope.sessionId)
	    {
		alert('dont login');
		return;
	    }

	    var resp = GroupService.getGroup({
		groupId: groupId
	    }, function ()
	    {
		if (resp.error_code === 0)
		{
		    $modal.open({
			templateUrl: 'views/groupdetail.html',
			controller: 'GroupDetailCtrl',
			resolve: {
			    group: function () {
				var group = resp.data;
				group.groupId = groupId;
				return group;
			    },
			    cb: function () {
				return $scope.updateDisplay;
			    }
			}
		    });
		}
		else
		{
		    ErrorCMDService.displayError(resp.error_message);
		}
	    });

	};

	$scope.clearGroup = function ()
	{
	    $scope.selectedGroup = 0;
	    $scope.groupMembers = [];
	    $scope.groupDoors = [];
	};

	$scope.pageGroupChanged = function () {
	    var start = ($scope.currentGroupPage - 1) * ($scope.itemPerPage + 1);
	    var apiResp = GroupService.listGroup({
		start: start,
		limit: $scope.itemPerPage
	    }, function () {

		if (apiResp.error_code === 0) {
		    var groups = apiResp.data.groups;
		    for (var i = 0; i < groups.length; ++i) {
			var d = new Date(groups[i].createTime * 1000);
			groups[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
			d = new Date(groups[i].lastUpdate * 1000);
			groups[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
		    }

		    $scope.groupList = groups;
		    $scope.groups = [].concat($scope.groupList);
		    $scope.totalGroupItems = apiResp.data.total;
		    $scope.totalGroupPages = $scope.totalGroupItems / $scope.itemPerPage + 1;

		    if ($scope.selectedGroup < start || $scope.selectedGroup > start + $scope.itemPerPage)
		    {
			$scope.clearGroup();
		    }

		}
		else
		{
		    ErrorCMDService.displayError(apiResp.error_message);
		}
	    });
	};


	$scope.getGroup = function () {

	    var memberResp = GroupService.getGroup({
		groupId: $scope.selectedGroup
	    },
	    function () {
		if (memberResp.error_code === 0) {
		    $scope.memberTable = memberResp.data.users;
		    $scope.doorTable = memberResp.data.doors;
		    $scope.groupMembers = [].concat($scope.memberTable);
		    $scope.groupDoors = [].concat($scope.DoorTable);

		}
		else {
		    ErrorCMDService.displayError(memberResp.error_message);
		}
	    }
	    );
	};
	$scope.updateDisplay = function () {
	    $scope.pageGroupChanged();
	    if ($scope.selectedGroup)
	    {
		$scope.getGroup();
	    }
	};

	$scope.addGroup = function () {
	    if (!$scope.sessionId)
	    {
		alert('dont login');
		return;
	    }

	    $modal.open({
		templateUrl: 'views/groupdetail.html',
		controller: 'GroupDetailCtrl',
		resolve: {
		    group: function () {
			return undefined;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};

	$scope.searchGroup = function ()
	{
	    if (!$scope.sessionId)
	    {
		alert('dont login');
		return;
	    }

	    alert('Dont support');
	};

	$scope.modalRegisterUser = function () {
	    if (!$scope.sessionId)
	    {
		alert('dont login');
		return;
	    }

	    if (!$scope.selectedGroup)
	    {
		alert('Please select group');
		return;
	    }

	    $modal.open({
		templateUrl: 'views/registerUserByGroup.html',
		controller: 'RegisterUserCtrl',
		resolve: {
		    selectedGroup: function ()
		    {
			return $scope.selectedGroup;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};

	$scope.modalRegisterDoor = function () {
	    if (!$scope.sessionId)
	    {
		alert('dont login');
		return;
	    }


	    if (!$scope.selectedGroup)
	    {
		alert('Please select group');
		return;
	    }

	    $modal.open({
		templateUrl: 'views/registerDoorByGroup.html',
		controller: 'RegisterDoorCtrl',
		resolve: {
		    selectedGroup: function ()
		    {
			return $scope.selectedGroup;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};


//	$scope.registerDoor = function () {
//
//	    if ($scope.selectedDoor === undefined)
//	    {
//		ErrorCMDService.displayError('missing params');
//		return;
//	    }
//
//	    var memberResp = GroupService.registerDoor({
//		groupId: $scope.selectedGroup,
//		uuid: $scope.selectedDoor.uuid,
//		major: $scope.selectedDoor.major,
//		minor: $scope.selectedDoor.minor
//	    },
//	    function () {
//
//		if (memberResp.error_code === 0) {
//		    $scope.groupDoors = memberResp.data;
//		    $scope.updateDisplay();
//		    $scope.selectedDoor = undefined;
//		}
//		else
//		{
//		    ErrorCMDService.displayError(memberResp.error_message);
//		}
//	    }
//	    );
//	};

	$scope.removeDoor = function (door) {

	    var memberResp = GroupService.unregisterDoor({
		groupId: $scope.selectedGroup,
		uuid: door.uuid,
		major: door.major,
		minor: door.minor
	    },
	    function () {

		if (memberResp.error_code === 0) {
		    $scope.groupDoors = memberResp.data;
		    $scope.updateDisplay();
		}
		else
		{
		    ErrorCMDService.displayError(memberResp.error_message);
		}
	    }
	    );
	};

	$scope.removeMember = function (userId) {

	    var memberResp = GroupService.unregisterUser({
		groupId: $scope.selectedGroup,
		userId: userId
	    },
	    function () {
		if (memberResp.error_code === 0) {
		    $scope.updateDisplay();
		}
		else
		{
		    ErrorCMDService.displayError(memberResp.error_message);
		}
	    }
	    );
	};


	$scope.init = function ()
	{
	    $scope.$route = $route;

	    $scope.itemPerPage = 5;

	    $scope.totalGroupItems = 0;
	    $scope.currentGroupPage = 1;
	    $scope.selectedGroup = 0;

	    $scope.sessionId = SessionService.getSessionId();
	    if ($scope.sessionId)
	    {
		$scope.updateDisplay();
	    }
	};
	$scope.init();
    }
]);