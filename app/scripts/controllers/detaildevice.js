'use strict';

var app = angular.module('iotapp');
app.controller('DetailDeviceCtrl', ['$scope', 'SessionService', 'ServiceConfig', 'GroupDeviceService',
	function ($scope, SessionService, ServiceConfig, GroupDeviceService) {
		$scope.init = function ()
		{
			iniButtonFirstGray();
			linechartIni();
			var sessionId = SessionService.getSessionId();
			var homeId = ServiceConfig.getParam('hgid');
			var groupId = ServiceConfig.getParam('groupId');
			var deviceId = ServiceConfig.getParam('deviceId');
			$scope.id = deviceId;

			$scope.showchart = true;



			var deviceType;
			var dataGroupDevice = GroupDeviceService.getGroupDevice({
				sessionId: sessionId,
				homeId: homeId,
				groupId: groupId
			}, function () {
				$scope.data = dataGroupDevice.data;
				for (var i = 0, max = $scope.data.length; i < max; i++) {
					for (var j = 0, maxlistdevice = $scope.data[i].devices.length; j < maxlistdevice; j++) {
						if ($scope.data[i].devices[j].id == deviceId) {
							$scope.deviceName = $scope.data[i].devices[j].name;
							$scope.description = $scope.data[i].devices[j].description;
							deviceType = $scope.data[i].devices[j].deviceType;
							switch (deviceType) {
								case 100:
									$(".showChart").hide();
									if ($scope.data[i].devices[j].properties.status == '1') {
										$scope.img = "light-on.png";
										$scope.stt = 'ON';
									}
									else
									{
										$scope.img = 'light-off.png';
										$scope.stt = 'OFF';
									}
									$scope.value = $scope.stt;
									
									break;
								case 105:
									$(".showChart").hide();
									if ($scope.data[i].devices[j].properties.status == '1') {
										$scope.img = 'open-door.png';
										$scope.stt = 'OPEN';
									}
									else
									{
										$scope.img = 'closed-door.png';
										$scope.stt = 'CLOSE';
									}
									$scope.value = $scope.stt;
									
									break;
								case 106:
									$scope.stt = 'VIEW CHART';
									$scope.value = $scope.data[i].devices[j].properties.value;
									$scope.img = 'nhietke.jpg';
									break;
								case 107:
									$scope.stt = 'VIEW CHART';
									$scope.value = $scope.data[i].devices[j].properties.value;
									$scope.img = 'nhietke.jpg';
									break;
								default:

									break;
							}
						}
					}
				}
			});

		};
		$scope.init();
		function iniButtonFirstGray()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-default';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		function iniButtonAllBule()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-info';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		$scope.changeViewChart = function (time)
		{

			iniButtonAllBule();
			switch (time) {
				case 'day':
					$scope.day = 'btn btn-minw btn-rounded btn-default';
					linechartIni();
					break;
				case 'week':
					$scope.week = 'btn btn-minw btn-rounded btn-default';
//                    $('#canvas').replaceWith('<canvas id="chart" width="100" height="600"></canvas>');
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataWeek, {
						responsive: true
					});
					break;
				case 'month':
					$scope.month = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataMonth, {
						responsive: true
					});
					break;
				case 'year':
					$scope.year = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataYear, {
						responsive: true
					});
					break;
				case 'realtime':
					$scope.realtime = 'btn btn-minw btn-rounded btn-default';
					liveChart();
					break;
				default:
					break;
			}
			animatedScroll();
		};
		function animatedScroll()
		{
			scroll(0, $(document).height());
		}
		var lineChartDataWeek = {
			labels: ["2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0"]
				}
			]

		};
		var lineChartDataMonth = {
			labels: ["2015-08-01", "2015-08-03", "2015-08-03", "2015-08-04", "2015-08-05", "2015-08-06", "2015-08-07", "2015-08-08", "2015-08-09", "2015-08-10", "2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "12", "100", "0", "11", "34", "12", "0", "12", "100", "0"]
				}
			]

		};
		var lineChartDataYear = {
			labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
				}
			]

		};

		function linechartIni()
		{
			var lineChartDataDate = {
				labels: ["00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", ],
				datasets: [
					{
						label: "My First dataset",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0", "12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
					}
				]

			};
			$('#canvas').remove();
			$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
			var ctx = document.getElementById("canvas").getContext("2d");
			window.myLine = new Chart(ctx).Line(lineChartDataDate, {
				responsive: true
			});
			$scope.action = function (id)
			{
				iniButtonFirstGray();
				var deviceType;
				for (var i = 0, max1 = $scope.data.length; i < max1; i++) {
					for (var j = 0, max = $scope.data[i].devices.length; j < max; j++) {
						if ($scope.data[i].devices[j].id == id) {
							deviceType = $scope.data[i].devices[j].deviceType;
							$scope.nameviewchart = $scope.data[i].devices[j].name;
							switch (deviceType) {
								case 100:
									$scope.valDisplay = "none";
									if ($scope.data[i].devices[j].properties.status == '0') {
										$scope.data[i].devices[j].img = 'light-on.png';
										$scope.data[i].devices[j].properties.status = '1';
										$scope.data[i].devices[j].stt = 'ON';
									}
									else
									{
										$scope.data[i].devices[j].img = 'light-off.png';
										$scope.data[i].devices[j].properties.status = '0';
										$scope.data[i].devices[j].stt = 'OFF';
									}
									break;
								case 105:
									$scope.valDisplay = "none";
									if ($scope.data[i].devices[j].properties.status == '0') {
										$scope.data[i].devices[j].img = 'open-door.png';
										$scope.data[i].devices[j].properties.status = '1';
										$scope.data[i].devices[j].stt = 'OPEN';
									}
									else
									{
										$scope.data[i].devices[j].img = 'closed-door.png';
										$scope.data[i].devices[j].properties.status = '0';
										$scope.data[i].devices[j].stt = 'CLOSE';
									}
									break;
								case 106:
									//Ve bieu do
									$scope.valDisplay = "";
									linechartIni();
									animatedScroll();
									//iniButtonFirstGray();

									break;
								case 107:
									//Ve bieu do
									$scope.valDisplay = "";
									linechartIni();
									animatedScroll();
									//iniButtonFirstGray();

									break;
								default:
									break;
							}
						}
					}
				}
			};
		}
	}
]);
