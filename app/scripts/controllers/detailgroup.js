'use strict';

var app = angular.module('iotapp');
app.controller('detailgroupCtrl', ['$scope', 'GroupDeviceService',
    function ($scope, GroupDeviceService) {
        $scope.init = function ()
        {
            $scope.title = 'Detail group';
            var id = getUrlVars()["id"];
            var name = getUrlVars()["name"];
            var resp = GroupDeviceService.getDeviceInGroup({
                groupId: id
            }, function () {
                $scope.data = resp.data;
                for (var i = 0, max1 = $scope.data.length; i < max1; i++) {
                    switch ($scope.data[i].deviceType) {
                        case 100:
                            if ($scope.data[i].properties.status == '1') {
                                $scope.data[i].img = 'light-on.png';
                                $scope.data[i].stt = 'ON';
                            }
                            else
                            {
                                $scope.data[i].img = 'light-off.png';
                                $scope.data[i].stt = 'OFF';
                            }
                            break;
                        case 105:
                            if ($scope.data[i].properties.status == '1') {
                                $scope.data[i].img = 'open-door.png';
                                $scope.data[i].stt = 'OPEN';
                            }
                            else
                            {
                                $scope.data[i].img = 'closed-door.png';
                                $scope.data[i].stt = 'CLOSE';
                            }

                            break;
                        default:
                            break;
                    }
                }
            });
        };
        $scope.init();
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                vars[key] = value;
            });
            return vars;
        }
        $scope.changeStt = function (id, onOff)
        {
            for (var i = 0, max1 = $scope.data.length; i < max1; i++) {
                switch ($scope.data[i].deviceType) {
                    case 100:
                        if ($scope.data[i].id == id) {
                            if ($scope.data[i].properties.status == '0') {
                                $scope.data[i].img = 'light-on.png';
                                $scope.data[i].properties.status = '1';
                                $scope.data[i].stt = 'ON';
                            }
                            else
                            {
                                $scope.data[i].img = 'light-off.png';
                                $scope.data[i].properties.status = '0';
                                $scope.data[i].stt = 'OFF';
                            }
                        }
                        break;
                    case 105:
                        if ($scope.data[i].id == id) {
                            if ($scope.data[i].properties.status == '0') {
                                $scope.data[i].img = 'open-door.png';
                                $scope.data[i].properties.status = '1';
                                $scope.data[i].stt = 'OPEN';
                            }
                            else
                            {
                                $scope.data[i].img = 'closed-door.png';
                                $scope.data[i].properties.status = '0';
                                $scope.data[i].stt = 'CLOSE';
                            }
                        }
                        break;
                    default:

                        break;
                }
            }
        };
    }
]);
