'use strict';

angular.module('iotapp')
.controller('RegisterDoorCtrl', ['$scope', '$modalInstance', 'selectedGroup', 'cb',
    'GroupService', 'ErrorCMDService',
    function ($scope, $modalInstance, selectedGroup, cb,
            GroupService, ErrorCMDService) {

        $scope.init = function ()
        {

        };
        $scope.init();

        $scope.ok = function () {
            $modalInstance.close($scope.localDoor);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirm = function () {

            if (!$scope.selectedDoor) {
                ErrorCMDService.displayError('missing params');
                return;
            }

            var memberResp = GroupService.registerDoor({
                groupId: selectedGroup,
                uuid: $scope.selectedDoor.uuid,
                major: $scope.selectedDoor.major,
                minor: $scope.selectedDoor.minor
            },
            function () {

                if (memberResp.error_code === 0) {
                    $scope.selectedDoor = undefined;
                    $scope.ok();
                    cb();
                }
                else
                {
                    ErrorCMDService.displayError(memberResp.error_message);
                }
            }
            );

        };
    }]);
