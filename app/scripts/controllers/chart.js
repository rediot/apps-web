'use strict';

var app = angular.module('iotapp');
app.controller('ChartCtrl', ['$scope', '$modal', '$resource', '$cookieStore', '$window',
    'SessionService', 'ServiceConfig', 'HomeGateWayService', 'UserService', 'ErrorCMDService',
    function ($scope, $modal, $resource, $cookieStore, $window,
            SessionService, ServiceConfig, HomeGateWayService, UserService, ErrorCMDService) {

        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100)
        };
        var lineChartData = {
            labels: ["Jan", "Feb", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
                }
            ]

        }

        window.onload = function () {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myLine = new Chart(ctx).Line(lineChartData, {
                responsive: true
            });
        }
        $scope.ini = function ()
        {
            
        }
        $scope.ini();
    }
]);