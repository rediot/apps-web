'use strict';

/**
 * @ngdoc function
 * @name iotapp.controller:ErrorcmdCtrl
 * @description
 * # ErrorcmdCtrl
 * Controller of the iotapp
 */
angular.module('iotapp')
	.controller('ErrorcmdCtrl', ['$scope', '$modalInstance', 'msg', '$timeout',
	    function ($scope, $modalInstance, msg, $timeout) {

		$scope.msgList = [];

		$scope.clearMsg = function () {
		    $scope.cancel();
		    $scope.msgList = [];
		};

		$scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		};

		$scope.ok = function () {
		    $modalInstance.close();
		};

		$scope.showMsg = function (msg) {
		    $scope.msgList = [{type: 'danger', content: msg}];
//		    $timeout($scope.cancel, 1000);
		};

		$scope.showMsg(msg);

	    }]);
