'use strict';

/**
 * @ngdoc function
 * @name iotapp.controller:SubmitmodalCtrl
 * @description
 * # SubmitmodalCtrl
 * Controller of the iotapp
 */
angular.module('iotapp')
	.controller('SubmitModalCtrl', ['$scope', '$modalInstance', 'cb',
	    function ($scope, $modalInstance, cb) {

		$scope.confirm = function () {
		    cb();
		    $scope.ok();
		};

		$scope.ok = function () {
		    $modalInstance.close();
		};
		$scope.cancel = function () {
		    $modalInstance.dismiss('cancel');
		};


	    }]);
