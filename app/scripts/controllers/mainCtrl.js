'use strict';

/**
 * @ngdoc function
 * @name upphotoviewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanplayApp
 */
var app = angular.module('iotapp');
app.controller('mainCtrl', ['$scope', '$modal', '$resource', '$cookieStore', '$window',
	'SessionService', 'HomeGateWayService', 'GroupDeviceService', 'ServiceConfig', 'UserService', 'ErrorCMDService',
	function ($scope, $modal, $resource, $cookieStore, $window,
			SessionService, HomeGateWayService, GroupDeviceService, ServiceConfig, UserService, ErrorCMDService) {

		$scope.ini = function () {

			$scope.uuname = "Welcom " + SessionService.getSessionUname();
			var resp = HomeGateWayService.getListHG({
			}, function () {
				$scope.listHG = resp.data.homes;
				$scope.hgid = resp.data.homes[0].id;
				var hgid = $scope.hgid;
				var sessionId = SessionService.getSessionId();

				var resplistGroup = GroupDeviceService.getGroupDevice({
					sessionId: sessionId,
					homeId: hgid
				}, function () {
					$scope.groupIdDefault = resplistGroup.data[0].id;
				});

				if ($scope.hgid == "") {
					$scope.logOut();
				}
			});
		};
		$scope.ini();

		$scope.logOut = function () {
			SessionService.logOut();
			window.location.href = ServiceConfig.getUrlLogin();
			$window.location.reload();
		};
		$scope.keypres = function ()
		{
			window.location.href = "/#/" + $scope.hgid + "/search?key=" + document.getElementById("search").value;
			
		};
	}
]);
