'use strict';

var app = angular.module('iotapp');
app.controller('addnewgroupCtrl', ['$scope', 'GroupDeviceService', 'SessionService', 'ServiceConfig',
	function ($scope, GroupDeviceService, SessionService, ServiceConfig) {
		$scope.init = function ()
		{
			var homeId = ServiceConfig.getParam('hgid');
			var sessionId = SessionService.getSessionId();
			var resp = GroupDeviceService.getGroupDevice({
				sessionId: sessionId,
				homeId: homeId
			}, function () {
				$scope.data = resp.data;
			});

			if (sessionId == '') {
				window.location.href = ServiceConfig.getUrlLogin();
			}
		};
		$scope.init();
		$scope.addgroup = function () {
			var homeId = $scope.homeID;
			var name = $scope.groupName;
			if (homeId.length > 0 & name.length > 0) {
				var resp = GroupDeviceService.addGroup({
					homeId: homeId,
					name: name
				}, function () {
					var resp = GroupDeviceService.getGroupDevice({
					}, function () {
						$scope.data = resp.data;
					});
				});
			}
			else
			{
				alert('data null');
			}
		};
		$scope.getGroupInformation = function (id, name, description) {
			$scope.edithomeID = id;
			$scope.editgroupName = name;
			$scope.editdescription = description;
		};
		$scope.editgroup = function () {
			var id = $scope.edithomeID;
			var name = $scope.editgroupName;
			var description = $scope.editdescription;
			var resp = GroupDeviceService.editGroup({
				homeId: id,
				name: name
			}, function () {
				//update lai thong tin list group-devices.
				var resp = GroupDeviceService.getGroupDevice({
				}, function () {
					$scope.data = resp.data;
				});
			});
		};
		$scope.deletegroup = function () {
			var groupId = $scope.edithomeID;
			var resp = GroupDeviceService.deleteGroup({
				groupId: groupId
			}, function (data) {
//                if (data.error ==0) {
//                    //xoa thanh cong
//                    alert('thanh cong');
//                }
//                else
//                {
//                    alert('khong thanh cong');
//                    //khong thanh cong
//                    
//                }

				//reload data
				var resp = GroupDeviceService.getGroupDevice({
				}, function () {
					$scope.data = resp.data;
				});
			});

		};
	}
]);
