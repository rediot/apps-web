'use strict';
var app = angular.module('iotapp');
app.controller('GroupDeviceCtrl', ['$cookieStore', '$scope', '$modal', '$route',
    'DoorService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route,
	    DoorService, ErrorCMDService, SessionService) {

	$scope.getAdmin = function ()
	{
	    return $cookieStore.get('user').isAdmin;
	};

	$scope.isAdmin = $scope.getAdmin();
    

	$scope.createDoor = function () {
	    $modal.open({
		templateUrl: 'views/doordetail.html',
		controller: 'DoorDetailCtrl',
		resolve: {
		    door: function () {
			return undefined;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};
	$scope.viewDoorDetail = function (uuid, major, minor)
	{
	    var resp = DoorService.getDoor({
		uuid: uuid,
		major: major,
		minor: minor
	    }, function ()
	    {
		if (resp.error_code === 0)
		{
		    $modal.open({
			templateUrl: 'views/doordetail.html',
			controller: 'DoorDetailCtrl',
			resolve: {
			    door: function () {
				var door = resp.data;
				door.uuid = uuid;
				door.major = major;
				door.minor = minor;
				return door;
			    },
			    cb: function () {
				return $scope.updateDisplay;
			    }
			}
		    });
		}
		else
		{
		    ErrorCMDService.displayError(resp.error_message);
		}

	    });
	};
	$scope.removeDoor = function (uuid, major, minor)
	{

	    var cb = function () {
		var resp = DoorService.removeDoor({
		    uuid: uuid,
		    major: major,
		    minor: minor

		}, function () {

		    if (resp.error_code === 0)
		    {
			$scope.updateDisplay();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.error_message);
		    }
		});
	    };
	    $modal.open({
		templateUrl: 'views/submitmodal.html',
		controller: 'SubmitModalCtrl',
		size: 'sm',
		resolve: {
		    cb: function () {
			return cb;
		    }
		}
	    });
	};
	$scope.pageChanged = function ()
	{
	    var start = ($scope.currentPage - 1) * $scope.itemPerPage;
	    var apiResp = DoorService.listDoor({
		start: start,
		limit: $scope.itemPerPage

	    }, function () {

		if (apiResp.error_code === 0) {
		    var doors = apiResp.data.doors;
		    for (var i = 0; i < doors.length; ++i) {
			var d = new Date(doors[i].createTime * 1000);
			doors[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
			d = new Date(doors[i].lastUpdate * 1000);
			doors[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
		    }
		    $scope.doorList = doors;
		    $scope.doors = [].concat($scope.doorList);
		    $scope.totalItems = apiResp.data.total;
		    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
		}
		else
		{
		    ErrorCMDService.displayError(apiResp.error_message);
		}
	    });
	};
	$scope.updateDisplay = function () {
	    $scope.pageChanged();
	};
	$scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	};
	$scope.init = function () {

	    $scope.$route = $route;
	    //Pagination
	    $scope.totalItems = 0;
	    $scope.itemPerPage = 10;
	    $scope.currentPage = 1;
	    var sessionId = SessionService.getSessionId();
	    if (sessionId)
	    {
		$scope.updateDisplay();
	    }
	};
	$scope.init();
    }
]);