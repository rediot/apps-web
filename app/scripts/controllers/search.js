'use strict';

/**
 * @ngdoc function
 * @name upphotoviewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanplayApp
 */
var app = angular.module('iotapp');
app.controller('SearchCtrl', ['$scope', '$modal', '$resource', '$cookieStore', '$window',
	'SessionService', 'HomeGateWayService', 'GroupDeviceService', 'ServiceConfig', 'UserService', 'ErrorCMDService',
	function ($scope, $modal, $resource, $cookieStore, $window,
			SessionService, HomeGateWayService, GroupDeviceService, ServiceConfig, UserService, ErrorCMDService) {

		$scope.ini = function () {
			$scope.key = document.getElementById("search").value;
			iniButtonAllBule();
			$(".showChart").hide();
			var search = $("#search-text").val();
			var resp = HomeGateWayService.getListHG({
			}, function () {
				$scope.listHG = resp.data.homes;
				$scope.hgid = resp.data.homes[0].id;
				var hgid = $scope.hgid;
				var sessionId = SessionService.getSessionId();

				var resplistGroup = GroupDeviceService.getGroupDevice({
					sessionId: sessionId,
					homeId: hgid
				}, function () {
					$scope.deviceingroup = resplistGroup.data[0].devices;
					$scope.data = resplistGroup.data;
					init_stt_img();
				});
			});			
		};
		$scope.ini();
		function init_stt_img()
		{
			var total = 0;
			for (var i = 0, totalDevices = $scope.data.length; i < totalDevices; i++) {
				total = total + $scope.data[i].devices.length;
				$scope.total = total;
			}
			for (var i = 0, maxData = $scope.data.length; i < maxData; i++) {
				for (var j = 0, maxDevice = $scope.data[i].devices.length; j < maxDevice; j++) {
					switch ($scope.data[i].devices[j].deviceType) {
						case 100:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = "light-on.png";
								$scope.data[i].devices[j].stt = 'ON';
							}
							else
							{
								$scope.data[i].devices[j].img = 'light-off.png';
								$scope.data[i].devices[j].stt = 'OFF';
							}
							$scope.data[i].devices[j].showImg = true;
							$scope.data[i].devices[j].showVal = false;
							break;
						case 105:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = 'open-door.png';
								$scope.data[i].devices[j].stt = 'OPEN';
							}
							else
							{
								$scope.data[i].devices[j].img = 'closed-door.png';
								$scope.data[i].devices[j].stt = 'CLOSE';
							}
							$scope.data[i].devices[j].showImg = true;
							$scope.data[i].devices[j].showVal = false;
							break;
						case 106:
							$scope.data[i].devices[j].img = '';
							$scope.data[i].devices[j].stt = 'VIEW CHART';
							$scope.data[i].devices[j].showImg = false;
							$scope.data[i].devices[j].showVal = true;
							break;
						case 107:
							$scope.data[i].devices[j].img = '';
							$scope.data[i].devices[j].stt = 'VIEW CHART';
							$scope.data[i].devices[j].showImg = false;
							$scope.data[i].devices[j].showVal = true;
							break;
						default:
							if ($scope.data[i].devices[j].properties.status == '1') {
								$scope.data[i].devices[j].img = 'open-door.png';
								$scope.data[i].devices[j].stt = 'OPEN';
							}
							else
							{
								$scope.data[i].devices[j].img = 'closed-door.png';
								$scope.data[i].devices[j].stt = 'CLOSE';
							}
							break;
					}
				}
			}
		}
		$scope.action = function (id, onOff)
		{
			iniButtonFirstGray();
			var deviceType;

			for (var i = 0, max1 = $scope.data.length; i < max1; i++) {
				for (var j = 0, max = $scope.data[i].devices.length; j < max; j++) {
					if ($scope.data[i].devices[j].id == id) {
						deviceType = $scope.data[i].devices[j].deviceType;
						$scope.nameviewchart = $scope.data[i].devices[j].name;
						switch (deviceType) {
							case 100:
								$(".showChart").hide();
								$scope.valDisplay = "none";
								if ($scope.data[i].devices[j].properties.status == '0') {
									$scope.data[i].devices[j].img = 'light-on.png';
									$scope.data[i].devices[j].properties.status = '1';
									$scope.data[i].devices[j].stt = 'ON';
								}
								else
								{
									$scope.data[i].devices[j].img = 'light-off.png';
									$scope.data[i].devices[j].properties.status = '0';
									$scope.data[i].devices[j].stt = 'OFF';
								}
								break;
							case 105:
								$(".showChart").hide();
								$scope.valDisplay = "none";
								if ($scope.data[i].devices[j].properties.status == '0') {
									$scope.data[i].devices[j].img = 'open-door.png';
									$scope.data[i].devices[j].properties.status = '1';
									$scope.data[i].devices[j].stt = 'OPEN';
								}
								else
								{
									$scope.data[i].devices[j].img = 'closed-door.png';
									$scope.data[i].devices[j].properties.status = '0';
									$scope.data[i].devices[j].stt = 'CLOSE';
								}
								break;
							case 106:
								$(".showChart").show();
								//Ve bieu do
								$scope.valDisplay = "";
								linechartIni();
								animatedScroll();
								//iniButtonFirstGray();

								break;
							case 107:
								$(".showChart").show();
								//Ve bieu do
								$scope.valDisplay = "";
								linechartIni();
								animatedScroll();
								//iniButtonFirstGray();

								break;
							default:
								break;
						}
					}
				}
			}

		};
		function animatedScroll()
		{
			scroll(0, $(document).height());
		}
		function iniButtonFirstGray()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-default';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		function iniButtonAllBule()
		{
			$scope.day = 'btn btn-minw btn-rounded btn-info';
			$scope.week = 'btn btn-minw btn-rounded btn-info';
			$scope.month = 'btn btn-minw btn-rounded btn-info';
			$scope.year = 'btn btn-minw btn-rounded btn-info';
			$scope.realtime = 'btn btn-minw btn-rounded btn-info';
		}
		var lineChartDataWeek = {
			labels: ["2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0"]
				}
			]

		};
		var lineChartDataMonth = {
			labels: ["2015-08-01", "2015-08-03", "2015-08-03", "2015-08-04", "2015-08-05", "2015-08-06", "2015-08-07", "2015-08-08", "2015-08-09", "2015-08-10", "2015-08-11", "2015-08-12", "2015-08-13", "2015-08-14", "2015-08-15", "2015-08-16", "2015-08-17"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "12", "100", "0", "11", "34", "12", "0", "12", "100", "0"]
				}
			]

		};
		var lineChartDataYear = {
			labels: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [
				{
					label: "My First dataset",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "rgba(220,220,220,1)",
					pointColor: "rgba(220,220,220,1)",
					pointStrokeColor: "#fff",
					pointHighlightFill: "#fff",
					pointHighlightStroke: "rgba(220,220,220,1)",
					data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
				}
			]

		};

		function linechartIni()
		{
			var lineChartDataDate = {
				labels: ["00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", ],
				datasets: [
					{
						label: "My First dataset",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: ["12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0", "12", "100", "0", "11", "34", "12", "0", "22", "90", "200", "100", "0"]
					}
				]

			};
			$('#canvas').remove();
			$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
			var ctx = document.getElementById("canvas").getContext("2d");
			window.myLine = new Chart(ctx).Line(lineChartDataDate, {
				responsive: true
			});
		}
		$scope.changeViewChart = function (time)
		{

			iniButtonAllBule();
			switch (time) {
				case 'day':
					$scope.day = 'btn btn-minw btn-rounded btn-default';
					linechartIni();
					break;
				case 'week':
					$scope.week = 'btn btn-minw btn-rounded btn-default';
//                    $('#canvas').replaceWith('<canvas id="chart" width="100" height="600"></canvas>');
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataWeek, {
						responsive: true
					});
					break;
				case 'month':
					$scope.month = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataMonth, {
						responsive: true
					});
					break;
				case 'year':
					$scope.year = 'btn btn-minw btn-rounded btn-default';
					$('#canvas').remove();
					$('#graph-container').append('<canvas id="canvas" height="100" width="600"><canvas>');
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartDataYear, {
						responsive: true
					});
					break;
				case 'realtime':
					$scope.realtime = 'btn btn-minw btn-rounded btn-default';
					liveChart();
					break;
				default:
					break;
			}
			animatedScroll();
		};
	}
]);
